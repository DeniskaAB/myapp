// роуты группы /department

const router = require('express').Router();
const PositionsController = require('../controllers/PositionsController');

// добавление данных должностей
router.post('/', PositionsController.post);

// получение списка должностей
router.get('/', PositionsController.index);

// изменение данных должностей
router.put('/:positionId', PositionsController.put);

// Удаление должностей с id
router.delete('/:positionId', PositionsController.delete);

module.exports = router;

// КОММЕНТЫ ПОМОШНИКИ
// router.get('/', (req, res) => {
//   res.status(200).json({ message: 'Select' });
// });
