// роуты группы /company

const router = require('express').Router();
const CompaniesController = require('../controllers/CompaniesController');

// добавление данных организации
router.post('/', CompaniesController.post);

// получение списка организаций
router.get('/', CompaniesController.index);

// изменение данных организации
router.put('/:companyId', CompaniesController.put);

// Удаление организации с id
router.delete('/:companyId', CompaniesController.delete);

module.exports = router;

// КОММЕНТЫ ПОМОШНИКИ
// router.get('/', (req, res) => {
//   res.status(200).json({ message: 'Select' });
// });
