// роуты группы /user
const router = require('express').Router();
const AuthenticationController = require('../controllers/AuthenticationController');

const AuthenticationControllerPolicy = require('../policies/AuthenticationControllerPolicy');

// роут регистрации нового пользователя
router.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register);
// (req, res) => {
//   res.status(200).json({ message: 'Sign in' });
// }

// роут авторизации нового пользователя
router.get('/login', (req, res) => {
    res.status(200).json({
        message: 'Login',
    });
});

module.exports = router;
