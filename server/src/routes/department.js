// роуты группы /department

const router = require('express').Router();
const DepartmentsController = require('../controllers/DepartmentsController');

// добавление данных подразделения
router.post('/', DepartmentsController.post);

// получение списка подразделения
router.get('/', DepartmentsController.index);

// изменение данных подразделения
router.put('/:departmentId', DepartmentsController.put);

// Удаление подразделения с id
router.delete('/:departmentId', DepartmentsController.delete);

module.exports = router;

// КОММЕНТЫ ПОМОШНИКИ
// router.get('/', (req, res) => {
//   res.status(200).json({ message: 'Select' });
// });
