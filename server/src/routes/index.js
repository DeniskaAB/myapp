// файл подключения роутов
// здесь же прописываются миндвари

const router = require('express').Router();

router.use('/user', require('./user.js')); // роуты для пользователей приложения
router.use('/company', require('./company.js')); // роуты для компаний
router.use('/employee', require('./employee.js')); // роуты для сотрудников
router.use('/department', require('./department.js')); // роуты для подразделений
router.use('/position', require('./position.js')); // роуты для должностей
router.use('/event', require('./event.js'));

// корневой роут
router.get('/', (req, res) => {
    res.status(200).json({ message: 'Root' });
});

// домашний роут
router.get('/home', (req, res) => {
    res.status(200).json({ message: 'Home' });
});

// редирект для не известных роутов
router.use((req, res) => {
    res.status(404).send('Роут не найден, иди лесом');
    // res.redirect('/');
});

module.exports = router;
