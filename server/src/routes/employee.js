// роуты группы /users

const router = require('express').Router();
const EmployeesController = require('../controllers/EmployeesController');

// роут получения списка сотрудников
// router.get('/all', (req, res) => {
//   res.status(200).json({
//     message: 'Select all',
//   });
// });

// роут получения списка сотрудников
router.get('/', EmployeesController.index);

// роут удаления записи сотрудника
// router.post('/remove', (req, res) => {
//   res.status(200).json({
//     message: 'Remove',
//   });
// });

// роут добавление данных сотрудника
router.post('/', EmployeesController.post);

// router.post('/test', EmployeesController.test);

module.exports = router;
