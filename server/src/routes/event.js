// роуты группы events

const router = require('express').Router();
const EventsController = require('../controllers/EventsController');

// роут получения списка сотрудников
router.get('/', EventsController.index);
router.post('/', EventsController.post);

module.exports = router;
