// файл конфигурации
const path = require('path');

module.exports = {
    port: process.env.PORT || 8081,
    db: {
        database: process.env.DB_NAME || 'tabtracker',
        user: process.env.DB_USER || 'tabtracker',
        password: process.env.DB_PASS || 'tabtracker',
        options: {
            dialect: process.env.DIALECT || 'sqlite',
            host: process.env.HOST || 'localhost',
            storage: path.resolve(__dirname, '../../tabtracker.sqlite'),
            // operatorsAliases: Sequelize.Op,
            logging: true,
            define: {
                charset: 'utf8',
                dialectOptions: {
                    collate: 'utf8_general_ci',
                },
            },
        },
    },
    authentication: {
        jwtSecret: process.env.JWT_SECRET || 'secret',
    },
};
