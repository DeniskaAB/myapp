const Sequelize = require('sequelize');

const {
    Employee,
} = require('../models');

module.exports = {
    // выборка по поиску или все
    async index(req, res) {
        try {
            let employees = null;

            console.log(req.query.search);

            const {
                search,
            } = req.query;
            console.log(search);

            if (search) {
                employees = await Employee.findAll({
                    where: {
                        $or: [
                            'fname', 'mname', 'lname',
                        ].map((key) => ({
                            [key]: {
                                [Sequelize.Op.like]: `%${search}%`,
                            },
                        })),
                    },

                    //  where: {
                    //   $or: [
                    //     'title', 'artist', 'genre', 'album'
                    //   ].map(key => ({
                    //     [key]: {
                    //       $like: `%${search}%`
                    //     }
                    //   }))

                });
            } else {
                employees = await Employee.findAll({
                    // limit: 10,
                });
            }
            res.send(employees);
        } catch (err) {
            // console.log(err);
            res.status(500).send({
                error: `произошла ошибка при выборке из базы данных сотрудников ${err}`,
            });
        }
    },

    // выборка по айди
    async show(req, res) {
        try {
            const employee = await Employee.findById(req.params.employeeId);
            res.send(employee);
        } catch (err) {
            res.status(500).send({
                error: `произошла ошибка при отображении данных сотрудника  ${err}`,
            });
        }
    },

    // добавление данных сотрудника
    // TODO: добавить валидацию переданных id отдела и организации
    // решить как выводить данную ошибку валидации,
    // так как в модели идет проверка наличия записей
    async post(req, res) {
        try {
            console.log(req.body.events);
            const employee = await Employee.create(req.body);
            res.send(employee);
            console.log(employee.id);
            console.log(Object.keys(req.body.events).length);

            // Object.keys(data).length;

            if (Object.keys(req.body.events).length !== 0) {
                //
                console.log('ЕСТЬ СОБЫТИЯ ДЛЯ СОТРУДНИКА', req.body.events.lenght);
            }
        } catch (err) {
            // TODO: пока верну тело ошибки
            res.status(500).send({
                error: `произошла ошибка при отображении данных сотрудника  ${err}`,
            });
        }
    },

    // изменение данных сотрудника
    async put(req, res) {
        try {
            await Employee.update(req.body, {
                where: {
                    id: req.params.employeeId,
                },
            });
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: `возникла ошибка при внесении изменений в данные сотрудника  ${err}`,
            });
        }
    },

    // async test(req, res) {
    //     try {
    //         console.log(req.body);
    //         await Employee.create({ name: 'nice owner', event: [{ name: 'nice property' },
    // { name: 'ugly property' }] }, { include: [Event] });
    //     } catch (err) {
    //         res.status(500).send({
    //             error: 'возникла ошибка при добавлении записи',
    //         });
    //         console.log(err);
    //     }
    // },
};
