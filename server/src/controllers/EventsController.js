const Sequelize = require('sequelize');

const {
    Event,
} = require('../models');

module.exports = {
    // добавление события
    async post(req, res) {
        try {
            const event = await Event.create(req.body);
            res.send(event);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при добавлении записи',
            });
        }
    },

    async index(req, res) {
        try {
            let events = null;
            const {
                search,
            } = req.query;
            if (search) {
                events = await Event.findAll({
                    where: {
                        name: {
                            [Sequelize.Op.like]: `%${search}%`,
                        },
                    },
                });
            } else {
                events = await Event.findAll({
                    // limit: 10,
                });
            }
            res.send(events);
        } catch (err) {
            res.status(500).send({
                error: 'произошла ошибка при выборке из базы данных организации',
            });
        }
    },

    // TODO: добавить другие эндпойнты
};
