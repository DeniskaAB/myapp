const Sequelize = require('sequelize');

const {
    Position,
} = require('../models');

module.exports = {
    // добавление данных организации
    async post(req, res) {
        try {
            console.log('<<<<<<');
            console.log(req.body);
            console.log('>>>>>>');
            let position = null;
            // const position = await Position.create(req.body);

            if (req.body instanceof Array) {
                position = await Position.bulkCreate(req.body);
            } else {
                position = await Position.create(req.body);
            }
            // bulkCreate для записи данных из массива const position
            // = await Position.create(req.body);

            console.log(position);
            res.send(position);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при добавлении записи',
                err,
            });
        }
    },

    // выборка по поиску или все
    async index(req, res) {
        try {
            let positions = null;
            const {
                search,
            } = req.query;
            if (search) {
                positions = await Position.findAll({
                    where: {
                        name: {
                            [Sequelize.Op.like]: `%${search}%`,
                        },
                    },
                });
            } else {
                positions = await Position.findAll({
                    // limit: 10,
                });
            }
            res.send(positions);
        } catch (err) {
            console.log(err);
            res.status(500).send({
                error: 'произошла ошибка при выборке из базы данных организации',
            });
        }
    },

    // изменение данных компании /company/:companyId
    async put(req, res) {
        try {
            await Position.update(req.body, {
                where: {
                    id: req.params.positionId,
                },
            });
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при внесении изменений в данные должности',
            });
        }
    },

    // удаление записи по идентификационному номеру /company/:companyId
    async delete(req, res) {
        try {
            await Position.destroy({
                where: {
                    id: req.params.positionId,
                },
            });
            console.log('***************************************');
            console.log(req.params.positionId);
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при удалении должности c id',
            });
        }
    },
};
