const { User } = require('../models');

module.exports = {
    async register(req, res) {
        try {
            const user = await User.create(req.body);
            const userJson = user.toJSON();
            res.send({
                user: userJson,
            });
        } catch (err) {
            // console.log(err);
            res.status(400).send({
                error: {
                    message: 'Ошибки пока джисоном будет посылать',
                    // err,
                },
            });
        }
    },
    // тут еще короче дальше другие экспортируемые методы
};
