const Sequelize = require('sequelize');

const {
    Company,
} = require('../models');

module.exports = {
    // добавление данных организации
    async post(req, res) {
        try {
            console.log(req.body);
            const company = await Company.create(req.body);
            // возвращаем в обратку
            res.send(company);
        } catch (err) {
            res.status(500).send({
                error: `возникла ошибка при добавлении записи${err}`,
            });
        }
    },

    // выборка по поиску или все
    async index(req, res) {
        try {
            let companies = null;
            const {
                search,
            } = req.query;
            if (search) {
                companies = await Company.findAll({
                    where: {
                        name: {
                            [Sequelize.Op.like]: `%${search}%`,
                        },
                    },
                });
            } else {
                companies = await Company.findAll({
                    // limit: 10,
                });
            }
            res.send(companies);
        } catch (err) {
            res.status(500).send({
                error: 'произошла ошибка при выборке из базы данных организации',
            });
        }
    },

    // изменение данных компании /company/:companyId
    async put(req, res) {
        try {
            await Company.update(req.body, {
                where: {
                    id: req.params.companyId,
                },
            });
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при внесении изменений в данные организации',
            });
        }
    },

    // удаление записи по идентификационному номеру /company/:companyId
    async delete(req, res) {
        try {
            await Company.destroy({
                where: {
                    id: req.params.companyId,
                },
            });
            console.log('***************************************');
            console.log(req.params.companyId);
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при удалении организации c id',
            });
        }
    },

};
