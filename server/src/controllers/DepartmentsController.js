const Sequelize = require('sequelize');

const {
    Department,
} = require('../models');

module.exports = {

    // добавление данных подразделения
    async post(req, res) {
        try {
            const department = await Department.create(req.body);
            res.send(department);
            console.log(department);
            console.log('*********************************************************************');
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при добавлении записи подразделения',
            });
            console.log(err);
        }
    },

    // выборка по поиску или все
    async index(req, res) {
        try {
            let departments = null;
            const {
                search,
            } = req.query;
            if (search) {
                departments = await Department.findAll({
                    where: {
                        name: {
                            [Sequelize.Op.like]: `%${search}%`,
                        },
                    },
                });
            } else {
                departments = await Department.findAll({
                    // limit: 10,
                });
            }
            res.send(departments);
        } catch (err) {
            res.status(500).send({
                error: 'произошла ошибка при выборке из базы данных организации',
            });
        }
    },

    // изменение данных подразделения /department/:departmentId
    async put(req, res) {
        try {
            await Department.update(req.body, {
                where: {
                    id: req.params.departmentId,
                },
            });
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при внесении изменений в данные организации',
            });
        }
    },

    // удаление записи по идентификационному номеру /department/:departmentId
    async delete(req, res) {
        try {
            await Department.destroy({
                where: {
                    id: req.params.departmentId,
                },
            });
            console.log('***************************************');
            console.log(req.params.departmentId);
            res.send(req.body);
        } catch (err) {
            res.status(500).send({
                error: 'возникла ошибка при удалении организации c id',
            });
        }
    },

};
