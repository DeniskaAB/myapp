module.exports = (sequelize, DataTypes) => {
    const Department = sequelize.define('Department', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Наименование отдела',
            validate: {},
        },
        // company_id: {
        // type: DataTypes.INTEGER,
        // allowNull: false,
        // comment: 'Связь с базой компаний', validate: {},
        // },
    }, {
        hooks: {},
    });

    Department.associate = function func(models) {
        Department.belongsTo(models.Company);
    };

    return Department;
};
