module.exports = (sequelize, DataTypes) => {
    const Position = sequelize.define('Position', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Наименование должности',
            validate: {},
        },
        is_leader: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            comment: 'Статус руководящая должность',
            validate: {},
        },
    }, {
        hooks: {},
    });

    return Position;
};
