/* данные
* Имя (fname)
* Фамилия (mname)
* Отчество (lname)

* Дата рождения (birthDate) новое
* (domicile) новое
* Пол (gender) новое
* hireDate (Дата трудоустройства) новое
* Должность (position), ссылочный тип
* Подразделение (department), ссылочный тип department_id
* Статус руководитель подразделения (is_leader)
* Освобожден от мероприятий (is_free)

Email (email) не нужно наверное, возможно лучше заменить на телефонный номер
Дата приема (employment_date) заменено на hireDate

перенести в отдельную модель, доставать из базы по связи с сотрудником
Удостоверение ()
  номер (certificate_number)
  истекает (certificate_till)

*/
module.exports = (sequelize, DataTypes) => {
    const Employee = sequelize.define('Employee', {
        fname: {
            comment: 'Имя',
            type: DataTypes.STRING,
            // allowNull: false,
            validate: {},
        },
        mname: {
            comment: 'Фамилия',
            type: DataTypes.STRING,
            // allowNull: false,
            validate: {},
        },
        lname: {
            comment: 'Отчество',
            type: DataTypes.STRING,
            // allowNull: false,
            validate: {},
        },
        birthDate: {
            comment: 'Дата рождения',
            type: DataTypes.DATEONLY,
            // allowNull: false,
            validate: {},
        },
        domicile: {
            comment: 'Адрес прописки',
            type: DataTypes.STRING,
            // allowNull: false,
            validate: {},
        },
        gender: {
            comment: 'Пол',
            type: DataTypes.STRING,
            // allowNull: false,
            validate: {},
        },
        hireDate: {
            comment: 'Дата трудоустройства',
            type: DataTypes.DATEONLY,
            // allowNull: false,
            validate: {},
        },
        is_leader: {
            comment: 'Статус руководитель подразделения',
            type: DataTypes.BOOLEAN,
            // allowNull: false,
            validate: {},
        },
        // position_id: {
        //   comment: 'Должность',
        //   type: DataTypes.INTEGER,
        //   // allowNull: false,
        //   validate: {},
        // },
        fired: {
            comment: 'статус увольнения',
            type: DataTypes.BOOLEAN,
            allowNull: false,
            validate: {},
        },
        is_free: {
            comment: 'освобожден от мероприятий',
            type: DataTypes.BOOLEAN,
            allowNull: false,
            validate: {},
        },
    // department_id: {
    //   comment: 'Связь с базой подразделений',
    //   type: DataTypes.INTEGER,
    //   // allowNull: false,
    //   validate: {},
    // },
    }, {
        ooks: {},
    });

    Employee.associate = function func(models) {
        Employee.belongsTo(models.Department);
        Employee.belongsTo(models.Position);
    };

    return Employee;
};
