module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Имя пользователя',
            validate: {},
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Адрес электронной почты',
            unique: true,
            validate: {},
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Пароль',
            validate: {},
        },
    }, {
        hooks: {},
    });

    return User;
};
