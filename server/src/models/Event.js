module.exports = (sequelize, DataTypes) => {
    const Event = sequelize.define('Event', {
        User_id: {
            type: DataTypes.INTEGER, allowNull: false, comment: 'Пользователь автор события', validate: {},
        },
        Comission_id: {
            type: DataTypes.INTEGER, allowNull: false, comment: 'Комиссия', validate: {},
        },
        created_at: {
            type: DataTypes.DATE, allowNull: false, comment: 'Дата создания', validate: {},
        },
        event_type: {
            type: DataTypes.INTEGER, allowNull: false, comment: 'Тип события', validate: {},
        },
        participants: {
            type: DataTypes.STRING, allowNull: false, comment: 'Участники комиссии', validate: {},
        },
        is_finished: {
            type: DataTypes.BOOLEAN, comment: 'Событие завершено', validate: {},
        },
    }, {
        hooks: {},
    });

    Event.associate = function func(models) {
        // Event.hasMany(models.Employee);
        Event.belongsToMany(models.Employee, { as: 'Employee', through: 'EventEmployee' });
        //  Employee.belongsTo(models.Position);
    };

    return Event;
};

// Member.belongsTo(Ship);
// Ship.hasMany(Member);
