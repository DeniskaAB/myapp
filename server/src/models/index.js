// Про отношения почитать здесь
// http://qaru.site/questions/228400/how-to-implement-many-to-many-association-in-sequelize

// для работы с базой данных

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../config/config');

const { Op } = Sequelize;
const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col,
};

// создаем пустой объект
const db = {};

// читаем конфигурацию
const sequelize = new Sequelize(
    config.db.database,
    config.db.user,
    config.db.password,
    config.db.options,
    { operatorsAliases },
);

// импортируем из всех файлы находящиеся в директории models описание моделей объектов баз данных
fs
    .readdirSync(__dirname) // читаем папки обходом
    .filter((file) => file !== 'index.js') // исключаем файл index.js
    .forEach((file) => { // из каждого файла импортируем модель в ОРМ
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// создаем отношение будет создана таблица связей События со множеством Сотрудников
// db.Event.belongsToMany(db.Employee, { as: 'Employee', through: 'EventEmployee' });

module.exports = db;
