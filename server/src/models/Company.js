/* данные модели
* Company {id, name}
* идентификационный номер, наименование организации
*
*/

module.exports = (sequelize, DataTypes) => {
    const Company = sequelize.define('Company', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Наименование организации',
            validate: {},
        },
    // TODO: возможно стоит добавить другие данные организации
    // удаление организации связано с целостностью связанных данных
    }, {
        hooks: {},
    });
    return Company;
};
