// точка входа в приложение
// читать про группировку роутов https://www.terlici.com/2014/09/29/express-router.html

// подключаем модули и зависимости
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

// ОРМ
const { sequelize } = require('./models');

// заргужаем конфигурационный файл
const config = require('./config/config');

const app = express();

app.use(morgan('combined')); // журнал
app.use(bodyParser.json()); // парсер
app.use(cors()); // корс

// подключаем все роуты к нашему приложению
app.use(require('./routes'));

// Включаем прослушивание
// TODO: добавить смену порта через конфг

sequelize.sync({ force: true })
    .then(() => {
        app.listen(config.port);
        console.log(`Server started on port ${config.port}`);
    });
