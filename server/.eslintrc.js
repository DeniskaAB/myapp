module.exports = {
    "extends": "airbnb-base","rules": {"no-console": 0},
    "rules": {
// Indent with 4 spaces
"indent": ["error", 4],

// Indent JSX with 4 spaces
//"react/jsx-indent": ["error", 4],

// Indent props with 4 spaces
//"react/jsx-indent-props": ["error", 4],
    },
}