import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },

        {
            path: '/prescriptions',
            name: 'prescriptions',
            component: () => import(/* webpackChunkName: "prescriptions" */ './views/Prescriptions.vue')
        },

        {
            path: '/employees',
            name: 'employees',
            component: () => import(/* webpackChunkName: "employees" */ './views/Employees.vue')
        },

        {
            path: '/learning',
            name: 'learning',
            component: () => import(/* webpackChunkName: "employees" */ './views/Learning.vue')
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '/DTtest',
            name: 'DTtest',
            component: () => import(/* webpackChunkName: "about" */ './views/DTtest.vue')
        },
        {
            path:'/positions',
            name:'positions',
            component: () => import('./views/Positions.vue')

        },
        {
            path:'/EmployeesAdd',
            name:'EmployeesAdd',
            component: () => import('./views/EmployeesAdd.vue')
        },
        {
            path: '/EventsAdd',
            name:'EventsAdd',
            component: () => import('./views/EventsAdd.vue')
        }
    ]
})
