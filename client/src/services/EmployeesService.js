/* eslint-disable no-unused-vars */
import Api from '@/services/Api';

export default {
  index(search) {
    return Api().get('employee', {
      params: {
        search: search
      }
    })
  },
  show(employeeId) {
    return Api().get(`employee/${employeeId}`)
  },
  post(employee) {
    return Api().post('employee', employee)
  },
  put(employee) {
     return Api().put(`employee/{employee.id}`, employee)
    // .then(
    //   (error) => {
    //     // eslint-disable-next-line
    //     console.log(error)
    //   });
  }
}