import Api from '@/services/Api';

export default {
    index(search) {
        return Api().get('event', {
            params: {
                search: search
            }
        })
    },
    show(eventId) {
        return Api().get(`event/${eventId}`)
    },
}