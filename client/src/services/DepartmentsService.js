import Api from '@/services/Api';

export default {
    index(search) {
        return Api().get('department', {
            params: {
                search: search
            }
        })
    },
    show(departmentId) {
        return Api().get(`department/${departmentId}`)
    },
    post(department) {
        return Api().post('department', department)
    },
    put(department) {
        return Api().put(`department/${department.id}`, department)
    }
}