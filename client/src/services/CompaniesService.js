import Api from '../services/Api';

export default {
    index(search) {
        return Api().get('company', {
            params: {
                search: search
            }
        })
    },
    show(companyId) {
        return Api().get(`company/${companyId}`)
    },
    post(company) {
        return Api().post('company', company)
    },
    put(company) {
        return Api().put(`company/${company.id}`, company)
    }
}