import Api from '@/services/Api';

export default {
    index(search) {
        return Api().get('position', {
            params: {
                search: search
            }
        })
    },
    show(positionId) {
        return Api().get(`position/${positionId}`)
    },
    post(position) {
        return Api().post('position', position)
    },
    put(position) {
        return Api().put(`position/${position.id}`, position)
    },
    delete(positionId) {
        return Api().delete( `position/${positionId.id}`)
    }
}
