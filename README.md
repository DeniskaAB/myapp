**Файл описания проекта**

*Проект содержит в себе два приложения. Серверное и клиентское.*

клиент прослушивает порт :8080
сервер прослушивает порт :3000


**Полезные ссылки**
В ссылке пример
https://codeburst.io/vue-crud-x-a-highly-customisable-crud-component-using-vuejs-and-vuetify-2b1539ce2054
https://github.com/ais-one/vue-crud-x/wiki

https://ru.vuejs.org/index.html
https://vuex.vuejs.org/ru/
https://vuetifyjs.com/ru/
https://vuex.vuejs.org/ru/installation.html

**Для получения последних версий модулей**
$ npm install -g npm-check-updates
$ ncu -u
$ ncu -ug
